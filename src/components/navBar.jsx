import React, { Component } from 'react';


class NavBar extends Component {
    state = {  }

    render() { 
        const styles={
            paddingLeft: '30px'
        };

        const {navs}= this.props;
        return ( 
            <nav style={styles} className="navbar navbar-expand-lg navbar-light bg-light">
                {/* <a style={{fontSize: '40px'}} className="navbar-brand" href="#">SAS</a> */}
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav">
                        {navs.map(nav => 
                            <a key={nav.id} className={
                                nav.active? "nav-item nav-link active":"nav-item nav-link"
                            } href="#">
                                <button className={
                                    nav.active?"btn btn-primary active":"btn btn-success"
                                }>{nav.heading}</button>
                            </a>

                        )}
                    </div>
                </div>
            </nav>
         );
    }
}
 
export default NavBar;