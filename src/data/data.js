export default  function getDataFromAPI(){   
    //this functions extracts data from API and returns it
    return {
        "first_name": "demo",   
        "last_name": "user",   
        "dob": "1990-10-01",   
        "avatar": "​https://picsum.photos/200/300​",   
        "roll_number": "61005",   
        "registration_number": "BCT005",   
        "batch": 8,   
        "year": 2061,   
        "faculty": "ELX and BCT",   
        "contact": {     
            "address": "somewhere in ktm",     
            "phone": "0401376666"   
        },   
        "parents": [     
            {       
                "name": "lazy father",       
                "avatar": "​https://picsum.photos/200/300​",       
                "address": {         
                    "contact": {           
                        "address": "somewhere in ktm",           
                        "phone": "1234567890"         
                    }       
                }     
            },     
            {       
                "name": "lazy mother",       
                "avatar": "​https://picsum.photos/200/300​", 
                "address": {         
                    "contact": {           
                        "address": "somewhere in ktm",           
                        "phone": "0987654321"         
                    }       
                }     
            }   
        ],   
        "guardians": [     
            {       
                "name": "lazy uncle",       
                "avatar": "​https://picsum.photos/200/300​",       
                "address": {         
                    "contact": {           
                        "address": "somewhere in ktm",           
                        "phone": "1234567890"         
                    }       
                }     
            },     
            {       
                "name": "lazy aunty",       
                "avatar": "​https://picsum.photos/200/300​",       
                "address": {         
                    "contact": {           
                        "address": "somewhere in ktm",           
                        "phone": "0987654321"         
                    }       
                }     
            }   
        ] 
    };
}