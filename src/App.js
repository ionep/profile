import React,{Component} from 'react';
import {connect} from 'react-redux';
import { addData } from './action';
import NavBar from './components/navBar';
import Profile from './components/profile';

// const data= getDataFromAPI();
class App extends Component {
  state = { 
    navs:[
      {id:1, heading: "Home", active: false},
      {id:2, heading: "Student Profile", active: true},
      {id:3, heading: "Administration", active: false},
      {id:4, heading: "Assignments", active: false}
    ]
   }

  componentDidMount(){
    //read data from API and send to store
    fetch("https://run.mocky.io/v3/291c5c08-af6d-42ac-bfae-65e575938138").
        then(response => response.json())
        .then(data=> this.props.addData(data))

  }

  render() { 
    return (  
      <React.Fragment>
        <NavBar navs={this.state.navs}/>
        <Profile />
      </React.Fragment>
    );
  }
}

const mapDispatchToProps= dispatch =>{
  return {
    addData: data=>dispatch(addData(data))
  };
} 

export default connect(null, mapDispatchToProps)(App);
