**This is a simple effort to read the data from the mock API and display using React and Redux**

The data is available at "https://run.mocky.io/v3/291c5c08-af6d-42ac-bfae-65e575938138" or also inside **data/data.js**

The code is written currently in **Javascript** but I plan to change it to TypeScript in the future.

---

## React Components

There are three components including the "App" component namely:

a. **App** located at **"src/App.js"** <br />

	This component is the parent component of the app. It has a **NavBar** and a **Profile** component inside of it. 
    It uses **redux** to dispatch an action to get the data from the data source mentioned above when this component 
    is mounted.

b. **NavBar** located at **"src/components/navBar.jsx"**

	This component is used to display the two navbars in the UI and takes properties **navs** which define the 
    various tab or links for each navigation bar. The properties included in **navs** are:
    1. id: Identification no
    2. heading: The text of the link
    3. active: A boolean to denote which link of the Navbar is selected or active

c. **Profile** located at **"src/components/profile.jsx"**

    This component implements a **Navbar** which can be seen in the UI. It uses **redux** to read the data from 
    the store and then display it in the UI.

---

## Redux files

There are 4 files for **redux** namely:

a. **Store** in "src/store.js"

    This file simply imports the **createStore** method from **redux** and creates a store using reducer.
    
b. **Reducer** in "src/reducer.js"

    This is the reducer file which takes care of only one **action** for now. It adds the state to the **store** 
    which is extracted from above mentioned link.
    
c. **Action** in "src/action.js"

    This file has an **action** method called **"addData"** which takes an argument "data" and has a **type: addData**
    and **payload: data** where data is the provided argument to the function.
    
d. **Action types** in "src/actionTypes.js"

    This file is used to list all the actions which is currently available and can be used to add actions easily in 
    the future.

---

## Used Libraries and Tools

   Name-Version
   
a. npm-6.14.5

b. react-16.13.1

c. redux-4.0.5

d. react-redux-7.2.0

e. bootstrap-4.5.0

---