import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import store from './store';
import {Provider} from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';

//create a subscriber to handle store changes
const unsubscribe= store.subscribe(() => {
  console.log("Store Changed!",store.getState());
});


ReactDOM.render(
  <Provider store={store}>
    <App />
  </ Provider>,
  document.getElementById('root')
);

