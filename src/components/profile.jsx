import React, { Component } from 'react';
import {connect} from 'react-redux';
import NavBar from './navBar';

class Profile extends Component {
    state = { 
        navs:[
            {id:1, heading: "Profile", active: true},
            {id:2, heading: "Previous Result", active: false},
            {id:3, heading: "Current Result", active: false},
          ]
     }

    render() { 
        const profileStyles={
            background:'#f8f9fa',
            marginTop :"0px",
            padding: '10px 10px 10px 20px',
            border: '1px solid black'
        };

        const gap={
            marginLeft: '30px'
        };

        if(this.props.payload!=null){
            //minimize the code to avoid writing this.props ecerytime
            const {payload}=this.props;
            return ( 
                <div className="jumbotron">
                    <div className="container">
                        <div className="row">
                            <div className='col-md-9'>
                                <h1>{payload.first_name} {payload.last_name}</h1>

                                <NavBar navs={this.state.navs}/> {/* The Navbar inside the profile */}
                                
                                {/* Profile Starts from here */}
                                <div style={profileStyles}>
                                    <div><b>Date of Birth:</b> {payload.dob}</div>
                                    <div><b>Roll Number:</b> {payload.roll_number}</div>
                                    <div><b>Registration Number:</b> {payload.registration_number}</div>
                                    <div><b>Batch:</b> {payload.batch}</div>
                                    <div><b>Year:</b> {payload.year}</div>
                                    <div><b>Faculty:</b> {payload.faculty}</div>
                                    <div>
                                        <b><u>Contact:</u></b> 
                                        <div style={gap}><b>Address:</b> {payload.contact.address}</div>
                                        <div style={gap}><b>Phone:</b> {payload.contact.phone}</div>
                                    </div>
                                    <div>
                                        <b><u>Parents:</u></b> 
                                        {payload.parents.map((parent,index) => 
                                            <div style={{marginBottom: '20px'}} className="row">
                                                <div className="col-md-6">
                                                {index+1}.<div style={gap}><b>Name:</b> {parent.name}</div>
                                                    <div style={gap}>
                                                        <b><u>Contact:</u></b> 
                                                        <div><b>Address:</b> {parent.address.contact.address}</div>
                                                        <div><b>Phone:</b> {parent.address.contact.phone}</div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                <div style={gap}><img src={parent.avatar} alt="Parent"/></div>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                    <div>
                                        <b><u>Guardians:</u></b> 
                                        {payload.guardians.map((guardian,index) => 
                                            <div style={{marginBottom: '20px'}} className="row">
                                                <div className="col-md-6">
                                                {index+1}.<div style={gap}><b>Name:</b> {guardian.name}</div>
                                                    <div style={gap}>
                                                        <b><u>Contact:</u></b> 
                                                        <div><b>Address:</b> {guardian.address.contact.address}</div>
                                                        <div><b>Phone:</b> {guardian.address.contact.phone}</div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                <div style={gap}><img src={guardian.avatar} alt="Guardian"/></div>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3'>
                                <img src={this.props.payload.avatar} alt="Student Avatar"/>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        else{
            return(
                <div className="container jumbotron">
                    <h2>No Data</h2>
                </div>
            );
        }
    }
}
const mapStateToProps= state =>{
    return {
      payload: state.payload,
    };
}
 
export default connect(mapStateToProps, null)(Profile);