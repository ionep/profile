import * as actions from './actionTypes';

export const addData= data => ({
    type: actions.ADD_DATA,
    payload: data,
});