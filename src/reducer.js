import * as actions from './actionTypes';

export default function reducer(state=[], action){
    switch(action.type){
        case actions.ADD_DATA:
            return {
                ...state,               //create a copy of state and add the payload and return the new state
                payload: action.payload
            };
        default:
            return state;
    }
}